<?php

use Illuminate\Database\Seeder;

class SocialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('socials')->insert([
            
             'name' => "facebook",
             'url' => "https://www.facebook.com/",
         ]);
           DB::table('socials')->insert([
            
             'name' => "twitter",
             'url' => "https://twitter.com/explore",
         ]);
           DB::table('socials')->insert([
            
             'name' => "instagarm",
             'url' => "https://www.instagram.com/",
         ]);
           DB::table('socials')->insert([
            
             'name' => "linkedin",
             'url' => "https://www.linkedin.com/",
         ]);
    }
}
