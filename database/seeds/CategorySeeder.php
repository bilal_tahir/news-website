<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            
             'name' => "News",
             'description' => "Pellentesque mattis arcu massa, nec fringilla turpis eleifend id.",
             'image' => "19.jpg",
             'slug' => "/blog/news"
         ]);

         DB::table('categories')->insert([
            
             'name' => "Entertainment",
             'description' => "Sed a elit euismod augue semper congue sit amet ac sapien.",
             'image' => "20.jpg",
             'slug' => "/blog/entertainment"
         ]);

         DB::table('categories')->insert([
            
             'name' => "Bussiness",
             'description' => "Pellentesque mattis arcu massa, nec fringilla turpis eleifend id.",
             'image' => "21.jpg",
             'slug' => "/blog/bussiness"
         ]);

         DB::table('categories')->insert([
            
             'name' => "Sports",
             'description' => "Augue semper congue sit amet ac sapien. Fusce consequat.",
             'image' => "22.jpg",
             'slug' => "/blog/sports"
         ]);

         DB::table('categories')->insert([
            
             'name' => "LifeStyle",
             'description' => "Pellentesque mattis arcu massa, nec fringilla turpis eleifend id.",
             'image' => "23.jpg",
             'slug' => "/blog/lifestyle"
         ]);

         DB::table('categories')->insert([
            
             'name' => "Opinion",
             'description' => "Augue semper congue sit amet ac sapien. Fusce consequat.",
             'image' => "24.jpg",
             'slug' => "/blog/opinion"
         ]);

         DB::table('categories')->insert([
            
             'name' => "Media",
             'description' => "Augue semper congue sit amet ac sapien. Fusce consequat.",
             'image' => "24.jpg",
             'slug' => "/blog/media"
         ]);

         DB::table('categories')->insert([
            
             'name' => "Live Tv/Radio",
             'description' => "Augue semper congue sit amet ac sapien. Fusce consequat.",
             'image' => "21.jpg",
             'slug' => "/blog/live"
         ]);

    }
}
