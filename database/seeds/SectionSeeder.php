<?php

use Illuminate\Database\Seeder;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('sections')->insert([
            
             'category_id' => 1,
             'name' => "Politics",
         ]);
         DB::table('sections')->insert([
            
             'category_id' => 1,
             'name' => "Entertainment",
         ]);
         DB::table('sections')->insert([
            
             'category_id' => 2,
             'name' => "Movies",
         ]);
         DB::table('sections')->insert([
            
             'category_id' => 2,
             'name' => "Music",
         ]);
         DB::table('sections')->insert([
            
             'category_id' => 3,
             'name' => "Economy",
         ]);
         DB::table('sections')->insert([
            
             'category_id' => 3,
             'name' => "Energy",
         ]);
         DB::table('sections')->insert([
            
             'category_id' => 4,
             'name' => "FootBall",
         ]);
         DB::table('sections')->insert([
            
             'category_id' => 4,
             'name' => "BasketBall",
         ]);
         DB::table('sections')->insert([
            
             'category_id' => 5,
             'name' => "Travel",
         ]);
         DB::table('sections')->insert([
            
             'category_id' => 5,
             'name' => "Cars",
         ]);
         DB::table('sections')->insert([
            
             'category_id' => 1,
             'name' => "Energy",
         ]);
         DB::table('sections')->insert([
            
             'category_id' => 5,
             'name' => "Cricket",
         ]);
         DB::table('sections')->insert([
            
             'category_id' => 1,
             'name' => "Pollution",
         ]);
         DB::table('sections')->insert([
            
             'category_id' => 3,
             'name' => "Finance",
         ]);
           DB::table('sections')->insert([
            
             'category_id' => 6,
             'name' => "General",
         ]);
          DB::table('sections')->insert([
            
             'category_id' => 7,
             'name' => "Photo Stories",
         ]);
        
    }
}
