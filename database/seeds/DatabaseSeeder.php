<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(SectionSeeder::class);
         $this->call(BlogSeeder::class);
          $this->call(VideoSeeder::class);
          $this->call(RadioSeeder::class);
          $this->call(LiveTvSeeder::class);
          $this->call(TermSeeder::class);
          $this->call(PolicySeeder::class);
          $this->call(ContactSeeder::class);
             $this->call(SocialSeeder::class);
              $this->call(LogoSeeder::class);
               $this->call(CommentSeeder::class);
    }
}
