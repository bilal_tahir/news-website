<?php

use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('contacts')->insert([
            
        'address' => "481 Creekside Beach, CA 93424",     
        'phone' => "123456789",
             'email' => "yourmain@gmail.com",
         ]);
    }
}
