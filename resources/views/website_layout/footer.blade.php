 <!-- ##### Footer Add Area Start ##### -->
    {{-- <div class="footer-add-area"> --}}
        {{-- <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="footer-add">
                        <a href="#"><img src="img/bg-img/footer-add.gif" alt=""></a>
                    </div>
                </div>
            </div>
        </div> --}}
    {{-- </div> --}}
    <!-- ##### Footer Add Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">

        <!-- Main Footer Area -->
        <div class="main-footer-area">
            <div class="container">
                <div class="row">

                    <!-- Footer Widget Area -->
                    {{-- <div class="col-12 col-md-4">
                        <div class="footer-widget-area mt-80">
                            <!-- Footer Logo -->
                            <div class="footer-logo">
                                <a href="index.html"><img src="img/core-img/logo.png" alt=""></a>
                            </div>
                            <!-- List -->
                            <ul class="list">
                                <li><a href="mailto:contact@youremail.com">contact@youremail.com</a></li>
                                <li><a href="tel:+4352782883884">+43 5278 2883 884</a></li>
                                <li><a href="http://yoursitename.com">www.yoursitename.com</a></li>
                            </ul>
                        </div>
                    </div> --}}

                    <!-- Footer Widget Area -->
                    <div class="col-12 col-md-4">
                        <div class="footer-widget-area mt-80">
                            <!-- Title -->
                            <h4 class="widget-title">Contact Us</h4>
                            <!-- List -->
                           <ul class="list">
                           <li><a >{{$con->address}}</a></li>
                                <li><a>{{$con->phone}}</a></li>
                                <li><a >{{$con->email}}</a></li>
                            </ul>
                        </div>
                    </div>

                     <div class="col-12 col-md-4">
                        <div class="footer-widget-area mt-80">
                            <!-- Title -->
                            <h4 class="widget-title">About Us</h4>
                            <!-- List -->
                            <ul class="list">
                            <li><a href="{{url('/live')}}">Radio/Tv</a></li>
                                
                                <li><a href="{{url('/policy')}}">Policy</a></li>
                                <li><a href="{{url('/terms')}}">Terms of Use</a></li>
                                <li><a href="{{url('/contact')}}">Contact Us</a></li>
                                
                            </ul>
                        </div>
                    </div>

                    <!-- Footer Widget Area -->
                    <div class="col-12 col-md-4">
                        <div class="footer-widget-area mt-80">
                            <!-- Title -->
                            <h4 class="widget-title">Social</h4>
                            <!-- List -->
                            <ul class="list">
                                
                                
                            <li><a href="{{$soc[0]->url}}" class="fa fa-facebook"> Facebook</a></li>
<li><a href="{{$soc[1]->url}}" class="fa fa-twitter"> Twitter</a></li>
 <li><a href="{{$soc[2]->url}}" class="fa fa-instagram"> Instagram</a></li>
<li><a href="{{$soc[3]->url}}" class="fa fa-linkedin"> Linkedin</a></li>
                                
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Bottom Footer Area -->
        <div class="bottom-footer-area">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <!-- Copywrite -->
                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved |<a href="https://colorlib.com" target="_blank">WeGlobe Technologies</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->
