<!doctype html>
<html lang="en">

@include('website_layout.head')

<body>



    @include('website_layout.header')
<!--Session Controller has no link -->
    <!-- Code for Session Model -->

    {{-- <div class = "popupcookie d-md-block" id="hide">
<p>This website use cookies to ensure better user experience.
</p>
<button onclick="sess()" class="cookie_button">Got It!</button>
</div> --}}

    @yield('content')

    @include('website_layout.footer')

     <!-- ##### All Javascript Files ##### -->
    <!-- jQuery-2.2.4 js -->
<script src="{{asset('js/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{asset('js/bootstrap/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>
    <!-- All Plugins js -->
    <script src="{{asset('js/plugins/plugins.js')}}"></script>
    <!-- Active js -->
    <script src="{{asset('js/active.js')}}"></script>
    @yield('page-level-js')
</body>

</html>