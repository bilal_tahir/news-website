 @extends('website_layout.main')
 @section('content')

    <!-- ##### Popular News Area Start ##### -->
    <div class="popular-news-area section-padding-80-50">
        <div class="container">




            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                    <h6>{{$term->heading}}</h6>
                    </div>

                    {!!html_entity_decode($term->description)!!}
                </div>
               

                </div>

              
            </div>
        </div>
    @endsection