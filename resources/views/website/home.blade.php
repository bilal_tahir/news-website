 @extends('website_layout.main')
 @section('content')
 <div class="container">
  <div class="slider">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    


      @foreach($sl as $sliders)
            @if($sliders->id == 1)
            
                  <div class="carousel-item active ">
                  <a href="{{$sliders->url}}"> <img class="d-block w-100 cusslider" src="{{ asset('storage') . '/'.$sliders->image}}" alt="First slide"></a>
            </div> 
            
            @else
             <div class="carousel-item ">
                <a href="{{$sliders->url}}"> <img class="d-block w-100 cusslider" src="{{ asset('storage') . '/'.$sliders->image}}" alt="Second slide"> </a>
            </div>
            @endif

           @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    </div>
</div>

 <div class="hero-area">
        <div class="container">
            <div class="row align-items-center">
               
                <!-- Hero Add -->
                <div class="col-12 col-md-6">
                    <div class="hero-add">
                        @if(isset($ad[0]))
                    <a href="{{$ad[0]->url}}"><img src="{{ asset('storage') . '/'.$ad[0]->image}}" style="display:block; margin: 0 auto; height: 160px !important; width:60% !important" alt=""></a>
                @endif    
                </div>
                </div>

                <!-- Hero Add -->
                <div class="col-12 col-md-6">
                    <div class="hero-add">
                        @if(isset($ad[1]))
                        <a href="{{$ad[1]->url}}"><img src="{{ asset('storage') . '/'.$ad[1]->image}}" style="display:block; margin: 0 auto; height: 160px !important; width:60% !important"  alt=""></a>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Featured Post Area Start ##### -->

    <div class="featured-post-area section-padding-80-50">
        <div class="container">
            <div class="row">
                
                <div class="col-12 col-md-6 col-lg-8">
                         <div class="section-heading">
                        <h6>Top Stories</h6>
                    </div>
                    <div class="row">

                     
@foreach($stor as $s)
                        <div class="col-6 col-md-4">
                            <!-- Single Featured Post -->
                            <div class="single-blog-post featured-post-2">
                                <div class="post-thumb">
                                    <a href="{{url('desc/'.$s->id)}}"><img src="{{ asset('storage') . '/'.$s->image}}" style="height:140px !important; width:180px !important;" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="{{url('desc/'.$s->id)}}" class="post-catagory">{{$s->category->name}}</a>
                                    <div class="post-meta">
                                        <a href="{{url('desc/'.$s->id)}}" class="post-title">
                                            <h6>{{$s->heading}}</h6>
                                        </a>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                     @endforeach   
                        

                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4">
                      <div class="section-heading">
                        <h6>Categories</h6>
                    </div>

                    @foreach($category as $cat)
                    <!-- Single Featured Post -->
                    <div class="single-blog-post small-featured-post d-flex">
                        <div class="post-thumb">
                        <a href="{{url('blog/'.$cat->id)}}"><img src="{{ asset('storage') . '/'.$cat->image}}" style="height:80px !important; width:85px !important;" alt=""></a>
                        </div>
                        <div class="post-data">
                        <a href="{{url('blog/'.$cat->id)}}}" class="post-catagory">{{$cat->name}}</a>
                            <div class="post-meta">
                                <a href="{{url('blog/'.$cat->id)}}" class="post-title">
                                <h6>{{$cat->description}}</h6>
                                </a>
                                {{-- <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p> --}}
                            </div>
                        </div>
                    </div>

                    
                    @endforeach
                     <div class="newsletter-widget">
                       <div class="hero-add">
                           @if(isset($ad[2]))
                       <a href="{{$ad[2]->url}}"><img src="{{ asset('storage') . '/'.$ad[2]->image}}" alt=""></a>
                       @endif
                    </div>
                    </div>

                    {{-- <!-- Single Featured Post -->
                    <div class="single-blog-post small-featured-post d-flex">
                        <div class="post-thumb">
                            <a href="#"><img src="storage/20.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-catagory">Politics</a>
                            <div class="post-meta">
                                <a href="#" class="post-title">
                                    <h6>Sed a elit euismod augue semper congue sit amet ac sapien.</h6>
                                </a>
                                <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                            </div>
                        </div>
                    </div>

                    <!-- Single Featured Post -->
                    <div class="single-blog-post small-featured-post d-flex">
                        <div class="post-thumb">
                            <a href="#"><img src="storage/21.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-catagory">Health</a>
                            <div class="post-meta">
                                <a href="#" class="post-title">
                                    <h6>Pellentesque mattis arcu massa, nec fringilla turpis eleifend id.</h6>
                                </a>
                                <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                            </div>
                        </div>
                    </div>

                    <!-- Single Featured Post -->
                    <div class="single-blog-post small-featured-post d-flex">
                        <div class="post-thumb">
                            <a href="#"><img src="storage/22.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-catagory">Finance</a>
                            <div class="post-meta">
                                <a href="#" class="post-title">
                                    <h6>Augue semper congue sit amet ac sapien. Fusce consequat.</h6>
                                </a>
                                <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                            </div>
                        </div>
                    </div>

                    <!-- Single Featured Post -->
                    <div class="single-blog-post small-featured-post d-flex">
                        <div class="post-thumb">
                            <a href="#"><img src="storage/23.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-catagory">Travel</a>
                            <div class="post-meta">
                                <a href="#" class="post-title">
                                    <h6>Pellentesque mattis arcu massa, nec fringilla turpis eleifend id.</h6>
                                </a>
                                <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                            </div>
                        </div>
                    </div>

                    <!-- Single Featured Post -->
                    <div class="single-blog-post small-featured-post d-flex">
                        <div class="post-thumb">
                            <a href="#"><img src="storage/24.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-catagory">Politics</a>
                            <div class="post-meta">
                                <a href="#" class="post-title">
                                    <h6>Augue semper congue sit amet ac sapien. Fusce consequat.</h6>
                                </a>
                                <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                            </div>
                        </div>
                    </div> --}}
                </div>
               
        </div>
    </div>
    <!-- ##### Featured Post Area End ##### -->

    <!-- ##### Popular News Area Start ##### -->
    <div class="popular-news-area section-padding-80-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h6>Videos</h6>
                    </div>

                    {{-- <div class="row">

                        <!-- Single Post -->
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="storage/12.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="storage/13.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="storage/14.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="storage/15.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>

                <!-- for ad-->

                {{-- <div class="col-12 col-lg-4">
                    <div class="section-heading">
                        <h6>Info</h6>
                    </div>
                    <!-- Popular News Widget -->
                    <div class="popular-news-widget mb-30">
                        <h3>4 Most Popular News</h3>

                        <!-- Single Popular Blog -->
                        <div class="single-popular-post">
                            <a href="#">
                                <h6><span>1.</span> Amet, consectetsur adipiscing elit. Nam eu metus sit amet odio sodales.</h6>
                            </a>
                            <p>April 14, 2018</p>
                        </div>

                        <!-- Single Popular Blog -->
                        <div class="single-popular-post">
                            <a href="#">
                                <h6><span>2.</span> Consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer.</h6>
                            </a>
                            <p>April 14, 2018</p>
                        </div>

                        <!-- Single Popular Blog -->
                        <div class="single-popular-post">
                            <a href="#">
                                <h6><span>3.</span> Adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo.</h6>
                            </a>
                            <p>April 14, 2018</p>
                        </div>

                        <!-- Single Popular Blog -->
                        <div class="single-popular-post">
                            <a href="#">
                                <h6><span>4.</span> Eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                            </a>
                            <p>April 14, 2018</p>
                        </div>
                    </div>

                    <!-- Newsletter Widget -->
                    <div class="newsletter-widget">
                        <h4>Newsletter</h4>
                        <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                        <form action="#" method="post">
                            <input type="text" name="text" placeholder="Name">
                            <input type="email" name="email" placeholder="Email">
                            <button type="submit" class="btn w-100">Subscribe</button>
                        </form>
                    </div>
                </div> --}}



<!-- for ad-->




            </div>
        </div>
    </div>
    <!-- ##### Popular News Area End ##### -->

    <!-- ##### Video Post Area Start ##### -->
    <div class="video-post-area bg-img bg-overlay" style="background-image: url(storage/bg1.jpg);">
         <div class="container">
              <div class="row justify-content-center"> 
                  <!-- Single Video Post --> {{-- <div class="col-12 col-sm-6 col-md-3"> 
                      <div class="single-video-post"> <img src="{{ asset('storage') . '/'.$v->image}}" 
                        style="height:240px !important; width:280px !important;" alt=""> 
                        <!-- Video Button --> <div class="videobtn"> 
                            <a href="{{$v->url}}" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                         </div> </div> </div> --}}
                         @foreach ($video as $v) 
                         <div class="col-12 col-md-6">
                              <iframe style="width: -webkit-fill-available; height:400px;" src="{{$v->url}}">
                        </iframe>
                     </div> 
                     @endforeach
                    </div>
                 </div> 
                </div>
    <!-- ##### Video Post Area End ##### -->

    <!-- ##### Editorial Post Area Start ##### -->
    <div class="editors-pick-post-area section-padding-80-50">
        <div class="container">
            <div class="row">
                <!-- Editors Pick -->
                <div class="col-12 
                {{-- col-md-7 col-lg-9 --}}
                ">
                    <div class="section-heading">
                        <h6>Recommended For You</h6>
                    </div>

                    <div class="row">

                        <!-- Single Post -->
                        @foreach($blog as $b)
                        <div class="col-6 col-lg-3">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="{{url('desc/'.$b->id)}}"><img src="{{ asset('storage') . '/'.$b->image}}" style="height:180px !important; width:200px !important;" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="{{url('desc/'.$b->id)}}" class="post-title">
                                    <h6>{{$b->heading}}</h6>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach


                    
                    </div>
                </div>

                <!-- World News -->
                {{-- <div class="col-12 col-md-5 col-lg-3">
                    <div class="section-heading">
                        <h6>World News</h6>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="storage/7.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="storage/8.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="storage/9.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="storage/10.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="#"><img src="storage/11.jpg" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-title">
                                <h6>Orci varius natoque penatibus et magnis</h6>
                            </a>
                            <div class="post-meta">
                                <div class="post-date"><a href="#">February 11, 2018</a></div>
                            </div>
                        </div>
                    </div>

                </div> --}}
            </div>
        </div>
    </div>
    <!-- ##### Editorial Post Area End ##### -->
      <div class="footer-add-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="footer-add">
                        {{-- @php
                         if(isset())   
                        @endphp --}}
                        @if(isset($ad[3]))
                        <a href="{{ $ad[3]->url }}"><img src="{{ asset('storage') . '/'.$ad[3]->image}}" class="footimg" style="height:200px !important; width:100% !important;" alt=""></a>
                    @endif
                        {{-- {{ isset($blogs[0]) ? $blogs[0]->title : '' }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection