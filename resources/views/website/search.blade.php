 @extends('website_layout.main')
 @section('content')
 {{-- <div class="hero-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-8">
                    <!-- Breaking News Widget -->
                    <div class="breaking-news-area d-flex align-items-center">
                        <div class="news-title">
                            <p>Breaking News</p>
                        </div>
                        <div id="breakingNewsTicker" class="ticker">
                            <ul>
                                <li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                                <li><a href="#">Welcome to Colorlib Family.</a></li>
                                <li><a href="#">Nam eu metus sitsit amet, consec!</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- Breaking News Widget -->
                    <div class="breaking-news-area d-flex align-items-center mt-15">
                        <div class="news-title title2">
                            <p>International</p>
                        </div>
                        <div id="internationalTicker" class="ticker">
                            <ul>
                                <li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                                <li><a href="#">Welcome to Colorlib Family.</a></li>
                                <li><a href="#">Nam eu metus sitsit amet, consec!</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- Hero Add -->
                <div class="col-12 col-lg-4">
                    <div class="hero-add">
                        <a href="#"><img src="img/bg-img/hero-add.gif" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Featured Post Area Start ##### -->

    <div class="featured-post-area section-padding-80-50">
        <div class="container">
            <div class="row">
                
                <div class="col-12 col-md-6 col-lg-8">
                         <div class="section-heading">
                        <h6>Search Results</h6>
                    </div>
                    <div class="row">

                     
@foreach($blog as $s)
                        <div class="col-12 col-md-4">
                            <!-- Single Featured Post -->
                            <div class="single-blog-post featured-post-2">
                                <div class="post-thumb">
                                    <a href="{{url('desc/'.$s->id)}}"><img src="{{ asset('storage') . '/'.$s->image}}" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="{{url('desc/'.$s->id)}}" class="post-catagory">{{$s->category->name}}</a>
                                    <div class="post-meta">
                                        <a href="{{url('desc/'.$s->id)}}" class="post-title">
                                            <h6>{{$s->heading}}</h6>
                                        </a>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                     @endforeach   
                        

                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection