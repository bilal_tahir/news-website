 @extends('website_layout.main')
 @section('content')

    <!-- ##### Popular News Area Start ##### -->
    <div class="popular-news-area section-padding-80-50">
        <div class="container">




            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                    <h6>{{$po->heading}}</h6>
                    </div>

                    {!!html_entity_decode($po->description)!!}
                </div>
               

                </div>

              
            </div>
        </div>
    @endsection