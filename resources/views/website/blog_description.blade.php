 @extends('website_layout.main')
 @section('content')
  <!-- ##### Blog Area Start ##### -->
    <div class="blog-area section-padding-0-80">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="blog-posts-area">

                        <!-- Single Featured Post -->
                        <div class="single-blog-post featured-post single-post">
                            <div class="post-thumb">
                                <a href="#"><img src="{{ asset('storage') . '/'.$d->image}}"  style="width:100% !important; height:100% !important" alt=""></a>
                            </div>
                            <div class="post-data">
                                <a  class="post-catagory">{{$d->category->name}}</a>
                                <a  class="post-title">
                                    <h6>{{$d->heading}}</h6>
                                </a>
                                <div class="post-meta">
                                    <p class="post-author">By <a>{{$d->written_by}}</a></p>
                                    
                                {!!html_entity_decode($d->description)!!}
                                </div>
                            </div>
                        </div>

                    

                      
                        <!-- Comment Area Start -->
                        <div class="comment_area clearfix">
                            <h5 class="title"> Comments</h5>

                            <ol>
                                @foreach($comm as $c)
                                <!-- Single Comment Area -->
                                <li class="single_comment_area">
                                    <!-- Comment Content -->
                                    <div class="comment-content d-flex">
                                        <!-- Comment Author -->
                                        {{-- <div class="comment-author">
                                            <img src="storage/30.jpg" alt="author">
                                        </div> --}}
                                        <!-- Comment Meta -->
                                        <div class="comment-meta">
                                        <a href="#" class="post-author">{{$c->name}}</a>
                                            {{-- <a href="#" class="post-date">April 15, 2018</a> --}}
                                        <p>{{$c->comment}}</p>
                                        </div>
                                    </div>
                                </li>
                                @endforeach

                            </ol>
                        </div>

                         @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif

                        <div class="post-a-comment-area section-padding-80-0">
                            <h4>Leave a comment</h4>

                            <!-- Reply Form -->
                            <div class="contact-form-area">
                                <form action="/addcomment" method="post">
                                    @csrf
                                    <div class="row">
                                        <input type="hidden" class="form-control" name="id" value={{$d->id}} placeholder="Name*">
                                        <div class="col-12 col-lg-6">
                                            <input type="text" class="form-control" name="name" placeholder="Name*">
                                        </div>
                                        <div class="col-12 col-lg-6">
                                            <input type="email" class="form-control" name="email" placeholder="Email*">
                                        </div>
                                       
                                        <div class="col-12">
                                            <textarea name="message" class="form-control" id="message" cols="30" rows="10" placeholder="Message"></textarea>
                                        </div>
                                        <div class="col-12 text-center">
                                            <button class="btn newspaper-btn mt-30 w-100" type="submit">Submit Comment</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="blog-sidebar-area">

                        <!-- Latest Posts Widget -->
                        <div class="latest-posts-widget mb-50">

                            <!-- Single Featured Post -->
                            @foreach($cat as $cat)
                            <!-- Single Featured Post -->
                    <div class="single-blog-post small-featured-post d-flex">
                        <div class="post-thumb">
                        <a href="{{url($cat->slug)}}"><img src="{{ asset('storage') . '/'.$cat->image}}" alt=""></a>
                        </div>
                        <div class="post-data">
                        <a href="{{url($cat->slug)}}" class="post-catagory">{{$cat->name}}</a>
                            <div class="post-meta">
                                <a href="{{url($cat->slug)}}" class="post-title">
                                <h6>{{$cat->description}}</h6>
                                </a>
                                {{-- <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p> --}}
                            </div>
                        </div>
                    </div>
@endforeach
                       
                         
                        </div>

                       
                  

                     

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Blog Area End ##### -->

  
 @endsection