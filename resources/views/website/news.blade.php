 @extends('website_layout.main')
 @section('content')
 <div class="popular-news-area section-padding-80-50">
        <div class="container">
            {{-- @php
            echo $count;
                @endphp --}}
            
            <div class="row">
                
                <div class="col-12 col-lg-8">
                    @php
                    $a = 0;
                    $b = 0;
                    $c = 0;

                    @endphp

                    @foreach($blog as $blog )
                    


                    @if($blog->section_id != $a)

                    @if($b != 0)

                    @php
                    // echo "Hello";
                    goto rex;
                   
                    @endphp
                    @php
                    done:
                    @endphp
@endif

                    @php
                    $a = $blog->section_id;
                    @endphp
                    <div class="section-heading">
                    <h6>{{$blog->section->name}}</h6>
                    </div>
                    


                    <div class="row">
                        @endif

                        <div class="col-6 col-md-3">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                <a href="{{url('desc/'.$blog->id)}}"><img src="{{ asset('storage') . '/'.$blog->image}}" alt="" style="height:120px !important; width:150px !important;"></a>
                                </div>
                                <div class="post-data">
                                    <a href="{{url('desc/'.$blog->id)}}" class="post-catagory">{{$blog->section->name}}</a>
                                    <a href="{{url('desc/'.$blog->id)}}"class="post-title">
                                        <h6>{{$blog->heading}}</h6>
                                    </a>
                                   
                                </div>
                            </div>
                        </div>
@if($c != 0)
                        @php
                        rex:
                        @endphp
                    </div>
                    @php
                    goto done;
                    @endphp

    @endif

    @php
    $b++;
    @endphp

                     @endforeach
                     
                </div>
            </div>
                

                <div class="col-12 col-lg-4">
                    <div class="section-heading">
                        <h6>Info</h6>
                    </div>
                    <!-- Popular News Widget -->
                    <div class="popular-news-widget mb-30">
                        <h3 style="background-color: black; color:white; text-align:center">Popular News</h3>

                        <!-- Single Popular Blog -->
                        @foreach($news as $n)
                        <div class="single-popular-post">
                        <a href="{{url('desc/'.$n->id)}}">
                            <p style="color:black !important; margin-left:5px !important; font-size:14px !important;"> {{$n->heading}}</p>
                            </a>
                        </div>
                        @endforeach

                      
                    </div>

                    <!-- Newsletter Widget -->
                   <div class="popular-news-widget mb-30">
                        <h3 style="background-color: black; color:white; text-align:center">Latest Stories</h3>

                        <!-- Single Popular Blog -->
                        @foreach($story as $n)
                        <div class="single-popular-post">
                        <a href="{{url('desc/'.$n->id)}}">
                            <p style="color:black !important; margin-left:5px !important; font-size:14px !important;"> {{$n->heading}}</p>
                            </a>
                        </div>
                        @endforeach

                      
                    </div>

                </div>
            </div>
        </div>
    </div>
   
 @endsection