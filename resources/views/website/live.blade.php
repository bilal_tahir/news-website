 @extends('website_layout.main')
 @section('content')

    <!-- ##### Popular News Area Start ##### -->
    <div class="popular-news-area section-padding-80-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h6>Live Radio</h6>
                    </div>

                    <div class="row">

                        <!-- Single Post -->
                        {{-- <div class="col-12 col-md-12">
                            <div class="single-blog-post style-3"> --}}
                                
                                 {{-- <div class="sgplayer" style="width:100%; height:300px; display:inline-block;"> --}}
    {{-- <iframe src="//player.streamguys.com/atunwa/mgl/joyfm/player.php?l=layout-small" frameBorder="0" scrolling="no" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" allow="autoplay" style="width:inherit; height:inherit; border:0;"></iframe> --}}
                                 {{-- </div> --}}
                            {{-- </div>
                        </div> --}}

                        <!-- Single Post -->
                        @foreach($radio as $rad)
                        <div class="col-12 col-md-3">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a><img src="{{ asset('storage') . '/'.$rad->image}}" alt="" style="border-radius: 50%; height:260px !important; width:300px !important;"></a>
                                </div>
                                <div class="post-data">
                                <iframe src="{{$rad->url}}" frameBorder="0" scrolling="no" allowfullscreen="true" webkitallowfullscreen="true"  allow="autoplay" style="width:inherit; height:inherit; border:0;"></iframe>
     
                                    
                                </div>
                            </div>
                        </div>
                        @endforeach


                        
                        <!-- Single Post -->
                        {{-- <div class="col-12 col-md-3">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="storage/14.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div> --}}

                        <!-- Single Post -->
                        {{-- <div class="col-12 col-md-3">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="#"><img src="storage/15.jpg" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">Finance</a>
                                    <a href="#" class="post-title">
                                        <h6>Dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placer. Sed varius leo ac...</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="img/core-img/like.png" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="img/core-img/chat.png" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>

              
            </div>



            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h6>Live Tv Streaming</h6>
                    </div>

                    <div class="row">

                        @foreach($tv as $tv)

                         <div class="col-12 col-sm-6 col-md-3">
                    <div class="single-video-post">
                        <img src="{{ asset('storage') . '/'.$tv->image}}" style="height:240px !important; width:280px !important;" alt="">
                        <!-- Video Button -->
                        <div class="videobtn">
                        <a href="{{$tv->url}}" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                @endforeach

                </div>

              
            </div>
        </div>
    </div>
</div>
    <!-- ##### Popular News Area End ##### -->
 @endsection