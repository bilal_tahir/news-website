<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','WebsiteController@home');


Route::get('/blog/1', 'BlogController@news');
Route::get('/blog/2', 'BlogController@entertainment');
Route::get('/blog/3', 'BlogController@bussiness');
Route::get('/blog/5', 'BlogController@lifestyle');
Route::get('/blog/4', 'BlogController@sports');
Route::get('/blog/6', 'BlogController@opinion');
Route::get('/live', 'BlogController@live');
Route::get('/blog/7', 'BlogController@media');

Route::get('/contact', 'ContactController@contact');

Route::get('/policy', 'PolicyController@policy');
Route::get('/terms', 'TermController@term');

Route::post('/message', 'MessageController@message');
Route::post('/addcomment', 'CommentController@add');
Route::post('/search', 'BlogController@search');

Route::get('/storage', function () {
Artisan::call('storage:link');
});

Route::get('desc/{id}','BlogController@desc');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
