<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Blog;
class Section extends Model
{
     public function categroy()
    {
        return $this->belongsTo(Category::class);
    }

     public function blog()
    {
        return $this->hasOne(Blog::class);
    }
}
