<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use App\Section;
use App\Video;
use App\Radio;
use App\LiveTv;
use App\Contact;
use App\Social;
use App\Logo;
use App\Comment;
use Illuminate\Http\Request;
use DB;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
    }

    public function news()
    {
        
             $head = Category::whereBetween('id', [1, 7])->get();
        $logo = Logo::where('id',1)->first(); 
        $con = Contact::where('id',1)->first();
    $soc = Social::whereBetween('id', [1, 4])->get();
        $blog = Blog::where('category_id',1)->orderBy('section_id', 'ASC')->get();
        $news = Blog::where([
       'category_id' => 1,
       'popular_news' => 1,
])->get();


$story = Blog::where([
       'category_id' => 1,
       'latest_story' => 1,
])->get();
        return view ('website.news')->with([
            'blog' => $blog,
            'news' => $news,
            'story' => $story,
            'con' => $con,
            'soc'=>$soc,
             'logo'=>$logo,
             'head' =>$head,
        ]);  
    }

     public function entertainment()
    {
        
             $head = Category::whereBetween('id', [1, 7])->get();
        $logo = Logo::where('id',1)->first(); 
        $con = Contact::where('id',1)->first();
    $soc = Social::whereBetween('id', [1, 4])->get();
        $blog = Blog::where('category_id',2)->orderBy('section_id', 'ASC')->get();
           $news = Blog::where([
       'category_id' => 2,
       'popular_news' => 1,
])->get();


$story = Blog::where([
       'category_id' => 2,
       'latest_story' => 1,
])->get();
        return view ('website.news')->with([
            'blog' => $blog,
            'news' => $news,
            'story' => $story,
            'con' => $con,
            'soc'=>$soc,
             'logo'=>$logo,
             'head' =>$head,
        ]);  
    }

     public function bussiness()
    {
        
             $head = Category::whereBetween('id', [1, 7])->get();
        $logo = Logo::where('id',1)->first(); 
        $con = Contact::where('id',1)->first();
    $soc = Social::whereBetween('id', [1, 4])->get();
        $blog = Blog::where('category_id',3)->orderBy('section_id', 'ASC')->get();
           $news = Blog::where([
       'category_id' => 3,
       'popular_news' => 1,
])->get();


$story = Blog::where([
       'category_id' => 3,
       'latest_story' => 1,
])->get();
        return view ('website.news')->with([
            'blog' => $blog,
            'news' => $news,
            'story' => $story,
            'con' => $con,
            'soc'=>$soc,
             'logo'=>$logo,
             'head' =>$head,
        ]);  
    }

     public function sports()
    {
        
             $head = Category::whereBetween('id', [1, 7])->get();
        $logo = Logo::where('id',1)->first(); 
        $con = Contact::where('id',1)->first();
    $soc = Social::whereBetween('id', [1, 4])->get();
        $blog = Blog::where('category_id',4)->orderBy('section_id', 'ASC')->get();
           $news = Blog::where([
       'category_id' => 4,
       'popular_news' => 1,
])->get();


$story = Blog::where([
       'category_id' => 4,
       'latest_story' => 1,
])->get();
        return view ('website.news')->with([
            'blog' => $blog,
            'news' => $news,
            'story' => $story,
            'con' => $con,
            'soc'=>$soc,
             'logo'=>$logo,
             'head' =>$head,
        ]);  
    }

     public function lifestyle()
    {
        
             $head = Category::whereBetween('id', [1, 7])->get();
        $logo = Logo::where('id',1)->first(); 
        $con = Contact::where('id',1)->first();
    $soc = Social::whereBetween('id', [1, 4])->get();
        $blog = Blog::where('category_id',5)->orderBy('section_id', 'ASC')->get();
           $news = Blog::where([
       'category_id' => 5,
       'popular_news' => 1,
])->get();


$story = Blog::where([
       'category_id' => 5,
       'latest_story' => 1,
])->get();
        return view ('website.news')->with([
            'blog' => $blog,
            'news' => $news,
            'story' => $story,
            'con' => $con,
            'soc'=>$soc,
             'logo'=>$logo,
             'head' =>$head,
        ]);  
    }

     public function opinion()
    {
        
             $head = Category::whereBetween('id', [1, 7])->get();
        $logo = Logo::where('id',1)->first(); 
        $con = Contact::where('id',1)->first();
    $soc = Social::whereBetween('id', [1, 4])->get();
        $blog = Blog::where('category_id',6)->orderBy('section_id', 'ASC')->get();
           $news = Blog::where([
       'category_id' => 6,
       'popular_news' => 1,
])->get();


$story = Blog::where([
       'category_id' => 6,
       'latest_story' => 1,
])->get();
        return view ('website.news')->with([
            'blog' => $blog,
            'news' => $news,
            'story' => $story,
            'con' => $con,
            'soc'=>$soc,
             'logo'=>$logo,
             'head' =>$head,
        ]);  
    }

     
    public function live()
    {
        
             $head = Category::whereBetween('id', [1, 7])->get();
        $logo = Logo::where('id',1)->first(); 
        $con = Contact::where('id',1)->first();
    $soc = Social::whereBetween('id', [1, 4])->get();
        // $blog = Blog::where('category_id',1)->orderBy('section_id', 'ASC')->get();
        // return view ('website.news')->with([
        //     'blog' => $blog,
        // 'news' => $news,
        //     'story' => $story,
        // ]);  
        $radio = Radio::all();
        $tv = LiveTv::all();
        return view ('website.live')->with([
            'radio' => $radio,
            'tv' => $tv,
            'con' => $con,
            'soc'=>$soc,
             'logo'=>$logo,
             'head' =>$head,
        ]);
    }

    public function desc($id)
    {
        
             $head = Category::whereBetween('id', [1, 7])->get();

        $logo = Logo::where('id',1)->first(); 
        $con = Contact::where('id',1)->first();
    $soc = Social::whereBetween('id', [1, 4])->get();
    $comm = Comment::where([
        'blog_id'=>$id,
        'post'=>1,
    ])->get();
    // dd($comm);
        $d = Blog::where('id',$id)->first();
        $cat = Category::whereBetween('id', [1, 8])->get();
        return view ('website.blog_description')->with([
            'd' => $d,
            'cat' => $cat,
            'con' => $con,
            'soc'=>$soc,
            'logo'=>$logo,
            'head' =>$head,
            'comm' => $comm,
        ]);
    }

        public function media()
    {
        
             $head = Category::whereBetween('id', [1, 7])->get();
        $logo = Logo::where('id',1)->first(); 
        $con = Contact::where('id',1)->first();
    $soc = Social::whereBetween('id', [1, 4])->get();
        $blog = Blog::where('category_id',7)->orderBy('section_id', 'ASC')->get();

           $news = Blog::where([
       'category_id' => 7,
       'popular_news' => 1,
])->get();


$story = Blog::where([
       'category_id' => 7,
       'latest_story' => 1,
])->get();
        return view ('website.news')->with([
            'blog' => $blog,
            'news' => $news,
            'story' => $story,
            'con' => $con,
            'soc'=>$soc,
             'logo'=>$logo,
             'head' =>$head,
        ]);  
    }

    public function search(Request $request)
    {
        $bl = Category::where('name', 'LIKE' ,"%$request->name%")->first();
        $blo = Section::where('name', 'LIKE' ,"%$request->name%")->first();
        if($bl)
        {
             
                 $head = Category::whereBetween('id', [1, 7])->get();
            $logo = Logo::where('id',1)->first(); 
        $con = Contact::where('id',1)->first();
    $soc = Social::whereBetween('id', [1, 4])->get();
            $blogs= Blog::where('category_id',$bl->id)->get();
            return view ('website.search')->with([
                'blog' => $blogs,
                'con' => $con,
            'soc'=>$soc,
             'logo'=>$logo,
             'head' =>$head,
            ]);
        }
        if($blo)
        {
             
                 $head = Category::whereBetween('id', [1, 7])->get();
            $logo = Logo::where('id',1)->first(); 
        $con = Contact::where('id',1)->first();
    $soc = Social::whereBetween('id', [1, 4])->get();
            $blogs= Blog::where('section_id',$blo->id)->get();
            return view ('website.search')->with([
                'blog' => $blogs,
                'con' => $con,
            'soc'=>$soc,
             'logo'=>$logo,
             'head' =>$head,
            ]);
        }
        return redirect()->back();
    }
    
}
