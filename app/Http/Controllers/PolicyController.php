<?php

namespace App\Http\Controllers;

use App\Policy;
use App\Contact;
use App\Social;
use App\Logo;
use App\Category;
use Illuminate\Http\Request;

class PolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Policy  $policy
     * @return \Illuminate\Http\Response
     */
    public function show(Policy $policy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Policy  $policy
     * @return \Illuminate\Http\Response
     */
    public function edit(Policy $policy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Policy  $policy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Policy $policy)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Policy  $policy
     * @return \Illuminate\Http\Response
     */
    public function destroy(Policy $policy)
    {
        //
    }
    public function policy()
    {
        $logo = Logo::where('id',1)->first();
         $con = Contact::where('id',1)->first();
              $head = Category::whereBetween('id', [1, 7])->get();
    $soc = Social::whereBetween('id', [1, 4])->get();
        $po = Policy::where('id',1)->first();
        return view ('website.policy')->with([
            'po' =>$po,
            'con' => $con,
            'soc'=>$soc,
             'logo'=>$logo,
             'head' =>$head,
        ]);

    }
}
