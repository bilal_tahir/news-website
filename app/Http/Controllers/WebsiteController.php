<?php

namespace App\Http\Controllers;

use App\Category;
use App\Blog;
use App\Video;
use App\Contact;
use App\Social;
use App\Logo;
use App\Slider;
use App\Adcense;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
     public function home()
    {
        $category = Category::whereBetween('id', [1, 8])->get();
        $con = Contact::where('id',1)->first();
        $video = Video::all();
        $blog = Blog::inRandomOrder()
    ->limit(6) // here is yours limit
    ->get();
    // dd($blog);
    $stor = Blog::where('top_story',1)->get();
    $soc = Social::whereBetween('id', [1, 4])->get();
    // dd($social);
        // $sec = Blog::find(1);
        // dd($sec->section);

        $head = Category::whereBetween('id', [1, 7])->get();

        $logo = Logo::where('id',1)->first();

        $ad = Adcense::whereBetween('id', [1, 4])->get();
        $sl = Slider::all();
        return view('website.home')->with([
            'category' => $category,
            'video' => $video,
            'blog' => $blog,
            'stor' => $stor,
            'con' => $con,
            'soc'=>$soc,
            'logo'=>$logo,
            'head' =>$head,
            'sl' => $sl,
            'ad' => $ad,
        ]);
    }
}
