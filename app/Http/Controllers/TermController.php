<?php

namespace App\Http\Controllers;
use App\Contact;
use App\Social;
use App\Term;
use App\Logo;
use App\Category;
use Illuminate\Http\Request;

class TermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Term  $term
     * @return \Illuminate\Http\Response
     */
    public function show(Term $term)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Term  $term
     * @return \Illuminate\Http\Response
     */
    public function edit(Term $term)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Term  $term
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Term $term)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Term  $term
     * @return \Illuminate\Http\Response
     */
    public function destroy(Term $term)
    {
        //
    }

     public function term()
    {
        $logo = Logo::where('id',1)->first();
        $con = Contact::where('id',1)->first();
    $soc = Social::whereBetween('id', [1, 4])->get();
         $head = Category::whereBetween('id', [1, 7])->get();
        $term = Term::where('id',1)->first();
        return view ('website.terms')->with([
            'term' =>$term,
            'con' => $con,
            'soc'=>$soc,
             'logo'=>$logo,
             'head' =>$head,
        ]);

    }
}
