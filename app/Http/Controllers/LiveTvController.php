<?php

namespace App\Http\Controllers;

use App\LiveTv;
use Illuminate\Http\Request;

class LiveTvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LiveTv  $liveTv
     * @return \Illuminate\Http\Response
     */
    public function show(LiveTv $liveTv)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LiveTv  $liveTv
     * @return \Illuminate\Http\Response
     */
    public function edit(LiveTv $liveTv)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LiveTv  $liveTv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LiveTv $liveTv)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LiveTv  $liveTv
     * @return \Illuminate\Http\Response
     */
    public function destroy(LiveTv $liveTv)
    {
        //
    }
}
