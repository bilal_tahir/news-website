<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Section;
use App\Blog;
class Category extends Model
{
     public function section()
    {
        return $this->hasMany(Section::class);
    }

     public function blog()
    {
        return $this->hasOne(Blog::class);
    }
}
