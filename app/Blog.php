<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Section;
class Blog extends Model
{
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

       public function section()
    {
        return $this->belongsTo(Section::class);
    }
}
